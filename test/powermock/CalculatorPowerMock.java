package powermock;

import homeworks.calculator.CalculatorHelen;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.testng.Assert;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalculatorHelen.class)
public class CalculatorPowerMock {

    @Test
    public void shouldMockStaticMethod() {
        PowerMockito.mockStatic(CalculatorHelen.class);

        Mockito.when(CalculatorHelen.staticMethod()).thenReturn(10);

        Assert.assertEquals(10, CalculatorHelen.staticMethod());
    }

    @Test
    public void shouldMockPrivateMethod() throws Exception {
        CalculatorHelen helen = new CalculatorHelen();

        CalculatorHelen calculatorHelen = PowerMockito.spy(helen);

        PowerMockito.doReturn(8).when(calculatorHelen, "privateMethod");

        Assert.assertEquals(8, calculatorHelen.callPrivate());
    }

    @Test
    public void shouldMockFinalMethod() {
        CalculatorHelen mock = PowerMockito.mock(CalculatorHelen.class);

        Mockito.when(mock.finalMethod()).thenReturn(7);

        Assert.assertEquals(7, mock.finalMethod());
    }
}
