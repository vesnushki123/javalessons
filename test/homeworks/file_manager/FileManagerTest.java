package homeworks.file_manager;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class FileManagerTest {

    private FileManager fileManager = new FileManager();

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Rule
    public SystemOutRule rule = new SystemOutRule().enableLog();

    @Test
    public void shouldCreateFile() throws IOException {

        String path = "./Test.txt";

        fileManager.createFile(path);

        Assert.assertTrue(Files.exists(Paths.get(path)));

        Assert.assertTrue(Files.isRegularFile(Paths.get(path)));

        fileManager.deleteFile(path);

    }

    @Test
    public void shouldCreateDirectory() throws IOException {

        String path = "./TestDirectory";

        fileManager.createDirectory(path);

        Path pathToDir = Paths.get(path);

        Assert.assertTrue(Files.exists(pathToDir));

        Assert.assertTrue(Files.isDirectory(pathToDir));

        fileManager.deleteDirectory(path);

    }

    @Test
    public void shouldConvertFile() throws IOException {

        File folder = this.folder.newFolder("TestDirectory");

        String path = folder.getPath();

        Path pathToPdf = Paths.get(path + "/Poem.pdf");

        String pathToTxt = path + "/Poem.txt";

        Files.write(Paths.get(pathToTxt), "Test".getBytes(), StandardOpenOption.CREATE);

        fileManager.conversionFile(pathToTxt);

        Assert.assertTrue(pathToPdf.endsWith("Poem.pdf"));
    }

    @Test
    public void shouldCopyFile() throws IOException {

        String oldPath = "./Test.txt";//try to use temporaryfolder rule

        String newPath = "./test/Test.txt";

        if (Files.notExists(Paths.get(oldPath))) {

            fileManager.createFile(oldPath);
        }

        fileManager.copyFile(oldPath, newPath);

        Assert.assertTrue(Files.exists(Paths.get(newPath)));

        fileManager.deleteFile(newPath);

    }

    @Test
    public void shouldDeleteFile() throws IOException {

        String path = "./Test.txt";

        if (Files.notExists(Paths.get(path))) {

            fileManager.createFile(path);
        }

        fileManager.deleteFile(path);

        Assert.assertFalse(Files.exists(Paths.get(path)));

    }

    @Test
    public void shouldDeleteDirectory() throws IOException {

        String path = "./testDirectory";

        if (Files.notExists(Paths.get(path))) {

            fileManager.createDirectory(path);
        }

        fileManager.deleteDirectory(path);

        Assert.assertFalse(Files.exists(Paths.get(path)));

    }

    @Test
    public void shouldRenameFileOrDirectory() throws IOException {

        String actualFileName = "./testDirectory";

        String expectedFileName = "./testDi";

        if (Files.notExists(Paths.get(actualFileName))) {

            fileManager.createDirectory(actualFileName);
        }

        fileManager.renameFileOrDirectory(actualFileName, expectedFileName);

        Assert.assertTrue(expectedFileName, true);

        fileManager.deleteDirectory(expectedFileName);

    }

    @Test
    public void shouldShowDirectoryContents() throws IOException {

        File file = folder.newFolder("TestDirectory", "Test", "Hello");

        Files.createFile(Paths.get(file.toPath().toString() + "/Test123.txt"));

        String actual =  fileManager.showDirectoryContents(file.toString());

        Assert.assertTrue(rule.getLog().contains(actual));
    }

}
