package homeworks.data;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Arrays;

@RunWith(PowerMockRunner.class)
@PrepareForTest(CalendarHW.class)
public class CalendarHWTest {

    private CalendarHW calendarHW = new CalendarHW();

    @Rule
    public SystemOutRule rule = new SystemOutRule().enableLog();

    @Before
    public void shouldClearEvents() {
        Event[] events = calendarHW.getEvents();

        for (int i = 0; i < events.length; i++) {

            events[i] = null;

        }
    }


    @Test
    public void shouldShowCurrentDateInTimeZone() {

        LocalDateTime dateTime = LocalDateTime.of(2018, 3, 20, 5, 0);

        PowerMockito.mockStatic(LocalDateTime.class);

        Mockito.when(LocalDateTime.now()).thenReturn(dateTime);

        calendarHW.showCurrentDateInTimeZone();

        Assert.assertTrue("Kiev time", rule.getLog().contains("2018-03-20T05:00"));

        Assert.assertTrue("Denver time", rule.getLog().contains("2018-03-19T21:00-06:00"));

        Assert.assertTrue("Magadan time", rule.getLog().contains("2018-03-20T14:00+11:00"));

    }

    @Test
    public void shouldCreateEvent() {

        Event[] events = calendarHW.getEvents();

        LocalDate now = LocalDate.now();

        String nameEvent = "New Event";

        calendarHW.createEvent(now, nameEvent);

        Event event = new Event(now, nameEvent);

        Assert.assertTrue(Arrays.asList(events).contains(event));
    }

    @Test
    public void shouldShowEvents() {

        LocalDate now = LocalDate.now();

        String nameEvent = "New Event";

        calendarHW.createEvent(now, nameEvent);

        calendarHW.showEvents();

        Assert.assertTrue(rule.getLog().contains(nameEvent));

    }

    @Test
    public void shouldShowEventsByDay() {

        LocalDate now = LocalDate.now();

        String event = "New Event";

        calendarHW.createEvent(now, event);

        calendarHW.showEventsByDay(now);

        Assert.assertTrue(rule.getLog().contains(now.toString()));

        Assert.assertTrue(rule.getLog().contains(event));

    }

    @Test
    public void shouldDeleteEvent() {

        LocalDate now = LocalDate.now();

        LocalDate lastDate = LocalDate.of(2000, 01, 01);

        String firstEvent = "First Event";

        String secondEvent = "Second Event";

        calendarHW.createEvent(now, firstEvent);

        calendarHW.createEvent(lastDate, secondEvent);

        calendarHW.deleteEvent(now);

        Assert.assertTrue(!rule.getLog().contains("First Event"));

    }

    @Test
    public void shouldShowTimeZoneAndDate() { ///check!!!

        String zone = "Australia/Sydney";

        LocalDateTime dateTime = LocalDateTime.of(2018, 3, 20, 5, 0, 11, 20);

        PowerMockito.mockStatic(LocalDateTime.class);

        Mockito.when(LocalDateTime.now(ZoneId.of(zone))).thenReturn(dateTime);

        calendarHW.showTimeZoneAndDate(zone);

        Assert.assertTrue(rule.getLog().contains("05:00:11"));

        Assert.assertTrue(rule.getLog().contains("TUESDAY"));


    }

    @Test
    public void shouldShowFutureDate() {

        LocalDate dateTime = LocalDate.of(2018, 4, 24);

        PowerMockito.mockStatic(LocalDate.class);

        Mockito.when(LocalDate.now()).thenReturn(dateTime);

        calendarHW.showFutureDate("week");

        calendarHW.showFutureDate("month");

        Assert.assertTrue(rule.getLog().contains("2018-05-01"));

        Assert.assertTrue(rule.getLog().contains("2018-05-24"));

    }

    @Test
    public void shouldShowDateOrTime() {

        LocalDate dateTime = LocalDate.of(2018, 4, 24);

        PowerMockito.mockStatic(LocalDate.class);

        Mockito.when(LocalDate.now()).thenReturn(dateTime);

        calendarHW.showDateOrTime("date");

        Assert.assertTrue(rule.getLog().contains("2018-04-24"));

    }

    @Test
    public void shouldShowDataFormat() {

        LocalDateTime dateTime = LocalDateTime.of(2018, 4, 24, 5, 0);

        PowerMockito.mockStatic(LocalDateTime.class);

        Mockito.when(LocalDateTime.now()).thenReturn(dateTime);

        calendarHW.showDateInFormat("yyyy.MM.dd");

        Assert.assertTrue(rule.getLog().contains("2018.04.24"));


    }

}