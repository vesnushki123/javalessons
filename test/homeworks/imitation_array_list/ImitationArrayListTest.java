package homeworks.imitation_array_list;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.testng.annotations.BeforeTest;

public class ImitationArrayListTest {
    private ImitationArrayList list = new ImitationArrayList(3);


    @BeforeTest
    public void beforeEachTest() {

        list.setArray(new int[1]);
    }

    @Test
    public void modifyElementByIndex() throws Exception {
        list.modifyElementByIndex(2, 1);

        int[] actualArray = list.getArray();

        Assert.assertEquals(2, actualArray[1]);
    }

}