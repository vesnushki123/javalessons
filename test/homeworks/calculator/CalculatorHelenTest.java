package homeworks;

import homeworks.calculator.CalculatorHelen;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@RunWith(MockitoJUnitRunner.class)
public class CalculatorHelenTest {

    @Mock
    CalculatorHelen calcMock;

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Rule
    public SystemOutRule outRule = new SystemOutRule().enableLog();


    @Test
    public void shouldTypeInConsole() {
        CalculatorHelen helen = new CalculatorHelen();

        helen.print();

//        Assert.assertEquals("Hello world\n", outRule.getLog());
        Assert.assertTrue(outRule.getLog().contains("Hello world"));
    }

    @Test
    public void shouldCreateFile() throws Exception {

        File file = folder.newFolder("FileManager", "Copy", "Lena");

        Files.createFile(Paths.get(file.toPath().toString() + "/Tets123.txt"));

        System.out.println(file.getAbsolutePath());
    }


    @Test
    public void testAdd() throws Exception {

        Mockito.when(calcMock.getFirstParam()).thenReturn(5);

        Mockito.when(calcMock.getSecondParam()).thenReturn(5);

        Mockito.when(calcMock.add()).thenCallRealMethod();

        Assert.assertEquals(10, calcMock.add());
    }

    @Test
    public void testSubtraction() throws Exception {

        Mockito.when(calcMock.getFirstParam()).thenReturn(20);

        Mockito.when(calcMock.getSecondParam()).thenReturn(10);

        Mockito.when(calcMock.subtraction()).thenCallRealMethod();

        Assert.assertEquals(10, calcMock.subtraction());
    }

    @Test
    public void testMultiplication() throws Exception {

        Mockito.when(calcMock.getFirstParam()).thenReturn(20);

        Mockito.when(calcMock.getSecondParam()).thenReturn(10);

        Mockito.when(calcMock.multiplication()).thenCallRealMethod();

        Assert.assertEquals(200, calcMock.multiplication());
    }

    @Test
    public void testDivision() throws Exception {

        Mockito.when(calcMock.getFirstParam()).thenReturn(20);

        Mockito.when(calcMock.getSecondParam()).thenReturn(10);

        Mockito.when(calcMock.division()).thenCallRealMethod();

        Assert.assertEquals(2, calcMock.division());
    }

    @Test
    public void testCalculationOfpercent() throws Exception {

        Mockito.when(calcMock.getFirstParam()).thenReturn(200);

        Mockito.when(calcMock.getSecondParam()).thenReturn(50);

        Mockito.when(calcMock.calculationOfpercent()).thenCallRealMethod();

        Assert.assertEquals(100, calcMock.calculationOfpercent());
    }

}