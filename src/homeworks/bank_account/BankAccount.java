package homeworks.bank_account;

public class BankAccount {

    private int accountSum;//0

    public BankAccount(int accountSum) {
       this.accountSum = accountSum;
    }

    public int getAccountSum() {
        return accountSum;
    }

    public void setAccountSum(int accountSum) {
        this.accountSum = accountSum;
    }

    public void withdrawMoney(int money) {

        int balance = accountSum - money;

        if (balance < 0) {
            System.out.println("Do not have enough money! " + balance);
            return;
        }

        accountSum -= money;
    }

    public void replenishAnAccount(int money) {

        accountSum += money;

        System.out.println("You have added money to account " + accountSum);

    }

    public static void main(String[] args) {

        int a = 10;

        a = a;

        a = 15;

        BankAccount bankAccount = new BankAccount(200);
        BankAccount bankAccount1 = bankAccount;
//        new BankAccount(200);
//        bankAccount = new BankAccount(500);

        char c = '\u0000';

        System.out.println("Before " + bankAccount.getAccountSum());

//        bankAccount.withdrawMoney(500);

//        System.out.println("After " + bankAccount.getAccountSum());

    }

}
