package homeworks.interface_abstract_classes.task_2;

import homeworks.interface_abstract_classes.task_2.reader.ReadFile;
import homeworks.interface_abstract_classes.task_2.writer.WriterFile;

import java.io.IOException;
import java.util.Scanner;

public class TestReaderWriter {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Write your sentences: ");

        String text = scanner.next();

        WriterFile fa = new WriterFile();

        fa.write(text);

        ReadFile rf = new ReadFile();

        rf.read();
    }
}
