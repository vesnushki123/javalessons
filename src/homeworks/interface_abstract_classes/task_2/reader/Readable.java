package homeworks.interface_abstract_classes.task_2.reader;

public interface Readable {
    void read();
}
