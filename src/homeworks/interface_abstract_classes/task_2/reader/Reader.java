package homeworks.interface_abstract_classes.task_2.reader;

import java.io.IOException;

public abstract class Reader implements Readable {
    public String modifyText(String text) throws IOException {

        String search = "I'm ready for writting to file";
        String replace = "I'm from file";
        return text.replaceAll(search, replace);
    }
}

