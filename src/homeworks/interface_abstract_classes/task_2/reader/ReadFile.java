package homeworks.interface_abstract_classes.task_2.reader;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ReadFile extends Reader {

    @Override
    public void read() {
        Path path = Paths.get("Poem.txt");

        String[] array = {"Hello", "Fun", "Gun"};

        String result = "";

        StringBuilder builder = new StringBuilder("");

        for (int i = 0; i < array.length; i++) {
//                result += array[i];
            builder.append(array[i]);
        }

//            builder.toString();

        try (BufferedReader br = Files.newBufferedReader(path)) {

            String text;

            while ((text = br.readLine()) != null) {

                System.out.println(modifyText(text));

            }

        } catch (IOException e) {

            e.printStackTrace();
        }

    }

}
