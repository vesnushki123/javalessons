package homeworks.interface_abstract_classes.task_2.writer;

import java.io.IOException;

public interface Writable {

    void write(String text) throws IOException;
}
