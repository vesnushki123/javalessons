package homeworks.interface_abstract_classes.task_2.writer;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class WriterFile extends Writer {

    @Override
    public void write(String text) throws IOException {
        Path path = Paths.get("Poem.txt");

        text = modifyText(text);

        Files.write(path, text.getBytes(), StandardOpenOption.WRITE, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);

        /*try (BufferedWriter writer = Files.newBufferedWriter(path)) {
            writer.write(text);

        } catch (IOException e) {

            e.printStackTrace();
        }*/
    }


}
