package homeworks.interface_abstract_classes.task_1;

public abstract class Figure implements Calculatable {

    private double sideSize;
    private double height;

    public Figure(double sideSize, double height) {
        this.sideSize = sideSize;
        this.height = height;
    }

    public double getSideSize() {
        return sideSize;
    }

    public double getHeight() {
        return height;
    }

//    @Override
//    public double calculateSquare() {
//        return height * sideSize;
//    }
}
