package homeworks.interface_abstract_classes.task_1;

public interface Calculatable {

    double calculateSquare();

    default void printInfo() {

        System.out.println(this);
    }

}


