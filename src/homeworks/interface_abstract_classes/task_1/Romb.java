package homeworks.interface_abstract_classes.task_1;

public class Romb extends Figure {

    public Romb(double size, double height) {

        super(size, height);

    }

    @Override
    public double calculateSquare() {
        return super.getHeight() * super.getSideSize();
    }

    @Override
    public String toString() {
        return "Romb";
    }

    /*@Override
    public void printInfo() {
        System.out.println("Romb");
    }*/

    public static void main(String[] args) {

        Romb romb = new Romb(2, 5);


        System.out.println(romb.calculateSquare());

        romb.printInfo();

    }
}
