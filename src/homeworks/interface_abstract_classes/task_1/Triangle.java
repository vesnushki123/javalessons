package homeworks.interface_abstract_classes.task_1;

public class Triangle extends Figure {


    public Triangle(double sideSize, double height) {
        super(sideSize * 0.5, height);
    }

    @Override
    public double calculateSquare() {

      return 0.5 * getSideSize() * getHeight();


    }
}
