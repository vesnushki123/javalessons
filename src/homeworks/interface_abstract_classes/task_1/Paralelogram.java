package homeworks.interface_abstract_classes.task_1;

public class Paralelogram extends Figure {

    public Paralelogram(double sideSize, double height) {
        super(sideSize, height);
    }

    @Override
    public double calculateSquare() {

     return  getSideSize() * getHeight();



    }
}
