package homeworks.array_task;

import java.util.Random;

public class Array {
    public static void main(String[] args) {
//        Создать два массива из 20 чисел. Первый массив проинициализировать четными числами.
//        Проинициализировать второй массив элементами первого массива при условии,
//        что индекс делится на 4 без остатка и элемент больше 3, но меньше 16.
//        Если условие не выполняется оставить элемент массива без изменения.

        int length = 20;

        int[] arrayOne = new int[length];

        int[] arrayTwo = new int[length];//0

        Random rn = new Random();

        for (int i = 0; i < length; i++) {

//            arrayOne[i] = rn.nextInt(20) * 2;
            arrayOne[i] = i * 2;
            System.out.print(arrayOne[i] + " ");
        }
// domain.company.product.module
        System.out.println();

        for (int i = 0; i < length; i++) {

            int element = arrayOne[i];

            if (element % 4 == 0 && element > 3 && element < 16) {
                arrayTwo[i] = element;
            }

//            System.out.print(arrayTwo[i] + " ");
        }

        /*Создать одномерный массив, проинициализировать его элементы случайными числами до 20 и найти подсчитать сколько раз
        в массиве встречается цифра 5.*/
        int[] array = new int[20];

        int count = 0;

        for (int i = 0; i < array.length; i++) {

            array[i] = rn.nextInt(20);

            if (array[i] == 5) {
                count++;
            }

            System.out.print(array[i] + " ");
        }
        System.out.println("Кол-во чисел 5 в массиве " + count);

    }
}

