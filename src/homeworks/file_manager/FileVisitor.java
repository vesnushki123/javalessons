package homeworks.file_manager;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

import static java.nio.file.FileVisitResult.CONTINUE;

public class FileVisitor extends SimpleFileVisitor<Path> {

    Path dir = Paths.get("./test/newDirectory");
    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        return CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        Files.delete(file);

        return CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {

        if (exc == null) {

            Files.delete(dir);

            return CONTINUE;

        } else {

            throw exc;
        }
    }


    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {

        return CONTINUE;
    }
}
