package homeworks.file_manager;

import java.util.Scanner;

class Menu {
    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {
        FileManager fileManager = new FileManager();

        while (true) {
            menu();
            int key = SCANNER.nextInt();
            try {
                switch (key) {
                    case 1: {

                        String path = getInfoFromUser("Enter name file");

                        fileManager.createFile(path);

                        break;
                    }
                    case 2: {
                        System.out.println("Enter directory name");

                        String path = SCANNER.next();

                        fileManager.createFile("./" + path);

                        break;
                    }
                    case 3: {
                        System.out.println("Enter file name for reading");

                        String path = SCANNER.next();

                        fileManager.readFile("./" + path);

                        break;
                    }
                    case 4: {
                        System.out.println("Enter file name for conversion");

                        String path = SCANNER.next();

                        fileManager.conversionFile("./" + path);

                        break;
                    }
                    case 5: {
                        System.out.println("Enter file name for copying");

                        String pathSource = SCANNER.next();

                        System.out.println("Enter path where you want to copy");

                        String pathTarget = SCANNER.next();

                        fileManager.copyFile("./" + pathSource, "./" + pathTarget);

                        break;
                    }
                    case 6: {
                        System.out.println("Enter file name for deleting");

                        String path = SCANNER.next();

                        fileManager.deleteFile("./" + path);

                        break;
                    }
                    case 7: {
                        System.out.println("Enter directory name for deleting");

                        String path = SCANNER.next();

                        fileManager.deleteDirectory("./" + path);

                        break;
                    }

                    case 8: {
                        System.out.println("Enter file or directory name for rename");

                        String pathOld = SCANNER.next();

                        System.out.println("Enter new file or directory name");

                        String pathNew = SCANNER.next();

                        fileManager.renameFileOrDirectory("./" + pathOld,"./" + pathNew);

                        break;
                    }

                    case 9: {
                        System.out.println("Enter directory name for seeing content");

                        String path = SCANNER.next();

                        fileManager.showDirectoryContents("./" + path);

                        break;
                    }

                    case 10:
                        return;
                    default:
                        return;
                }
            } catch (Exception e) {
                System.out.println("Exception" + e);
            }
        }
    }

    public static void menu() {
        System.out.println(
                "1) Create file\n" +
                "2) Create directory\n" +
                "3) Read file\n" +
                "4) Conversion file\n" +
                "5) Copy file\n" +
                "6) Delete file \n" +
                "7) Delete directory\n" +
                "8) Rename file\n" +
                "9) See what contains directory\n" +
        "10) Exit");
    }

    private static String getInfoFromUser(String message) {
        System.out.println(message);
        return SCANNER.next();
    }
}
