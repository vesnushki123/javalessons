package homeworks.file_manager;

import java.io.IOException;

public class TestException extends IOException {

    private String message;

    public TestException(String message) {
        this.message = message;
    }

    public void printMessage() {
        System.out.println(message);
    }

}
