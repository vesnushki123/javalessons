package homeworks.file_manager;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.*;

public class FileManager {

    public static final String EXTENSION_TXT = ".txt";

    public static final String PATH_TO_DIR = "./";
    public static final String EXTENSION_PDF = ".pdf";


    public void createFile(String path) throws IOException {

        Path pathToFile = FileSystems.getDefault().getPath(PATH_TO_DIR + path + EXTENSION_TXT);

        if (!Files.exists(pathToFile)) {

            Files.createFile(pathToFile);
        }
    }

    public void createDirectory(String path) throws IOException {

        Path pathToDirectory = FileSystems.getDefault().getPath(path);

        Files.createDirectory(pathToDirectory);

    }

    public String readFile(String path) throws IOException {

        return new String(Files.readAllBytes(Paths.get(path)));

    }


    public void conversionFile(String path) throws IOException {

        Document document = new Document();

        //get name of file from path "./GitKeyMaps.txt"

        int positionDotSlash = path.lastIndexOf("/");

        int positionExtension = path.indexOf(EXTENSION_TXT);

        String nameOfFile = path.substring(positionDotSlash + 1, positionExtension);

        try {

            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(PATH_TO_DIR + nameOfFile + EXTENSION_PDF));

            document.open();

            String text = readFile(path);

            document.add(new Paragraph(text));

            document.close();

            writer.close();

        } catch (DocumentException e) {

            e.printStackTrace();

        } catch (FileNotFoundException e) {

            e.printStackTrace();
        }

    }

    // recheck !!!!
    public void copyFile(String pathSource, String pathTarget) throws IOException {

        Path pathOld = FileSystems.getDefault().getPath(pathSource);

        Path pathNew = Paths.get(pathTarget);

        try {

            if (Files.notExists(pathOld) || Files.notExists(pathNew)) {

                throw new TestException("File " + pathOld.toFile().getName() + " doesn't exist");
            }

            Files.copy(pathOld, pathNew, StandardCopyOption.REPLACE_EXISTING);

        } catch (TestException e) {
            e.printMessage();
            createFile(pathOld.toString());

        }

    }


    public void deleteFile(String path) throws IOException {

        Path pathToFile = FileSystems.getDefault().getPath(path);

        if (Files.exists(pathToFile)) {

            Files.delete(pathToFile);
        }

    }

    public void deleteDirectory(String path) throws IOException {

        Path pathToDirectory = FileSystems.getDefault().getPath(path);

        Files.walkFileTree(pathToDirectory, new DeleteVisitor());

    }

    public void renameFileOrDirectory(String oldName, String newName) throws IOException {

        Files.move(Paths.get(oldName), Paths.get(newName), StandardCopyOption.REPLACE_EXISTING);

    }


    public String showDirectoryContents(String path) throws IOException {

        Path pathToDirectory = FileSystems.getDefault().getPath(path);

        Files.walkFileTree(pathToDirectory, new ShowContentVisitor());

        return path;
    }

    public static void main(String[] args) throws IOException {

        FileManager fm = new FileManager();

        fm.conversionFile("./Poem.txt");

    }
}




