package homeworks.ss_taks_with_inheritance;

//Please create an abstract class Shape and two subclasses Square and Circle. Replace code in method drawShape()
// according to principles of polymorphism. Please propose the other solutions for improving quality of the code.
public abstract class Shape {

    public void drawShape() {
        System.out.println("Some figure");
    }

    //others methods
}


