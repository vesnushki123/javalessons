package homeworks.imitation_array_list;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.Date;


public class ImitationArrayList {

    private final Path path = Paths.get("Log.txt");

    private int[] array;

    public ImitationArrayList(int size) {

        array = new int[size];//0
    }

    //    TODO only for tests
    public void setArray(int[] array) {
        this.array = array;
    }

    //    TODO only for tests
    public int[] getArray() {
        return array;
    }

    public void addElement(int value) throws IOException {

        String log = new Date() + "\tmethod -> addElement" + "\tparams -> " + value + "\n";

        writeLogToFile(log);

        resize();

        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                array[i] = value;
                break;
            }
        }

    }

    private void resize() throws IOException {

        String log = new Date() + "\tmethod -> resize" + "\tparams -> " + "\n";

        writeLogToFile(log);

        boolean shouldResize = false;

        for (int element : array) {
            if (element == 0) {
                shouldResize = true;
                break;
            }
        }

        if (!shouldResize) {
            int[] temp = new int[array.length * 2];//0

            for (int i = 0; i < array.length; i++) {
                temp[i] = array[i];
            }

            array = temp;
        }
    }

    public void modifyElementByIndex(int newValue, int index) throws IOException {

        String log = new Date() + "\tmethod -> modifyElementByIndex" + "\tparams -> " + newValue + "\tparams -> " + index + "\n";

        writeLogToFile(log);

        if (index < array.length) {

            array[index] = newValue;

        } else {//delete

            System.out.println("Array length less that entered index!");
        }

    }


    public void deleteElementByIndex(int index) throws IOException {

        String log = new Date() + "\tmethod -> deleteElementByIndex" + "\tparams -> " + index + "\n";

        writeLogToFile(log);

        if (index >= array.length) {

            System.out.println("Array length less that entered index!");

            return;

        }

        int[] tempArray = new int[array.length - 1];//0

        for (int i = 0; i < index; i++) {
            tempArray[i] = array[i];
        }

        for (int i = index; i < array.length - 1; i++) {
            tempArray[i] = array[i + 1];
        }

        array = tempArray;

    }

    public void increaseArray(int qtyOfIncreasing) throws IOException {

        String log = new Date() + "\tmethod -> increaseArray" + "\tparams -> " + qtyOfIncreasing + "\n";

        writeLogToFile(log);


        int[] temp = new int[array.length + qtyOfIncreasing];

        for (int i = 0; i < array.length; i++) {

            temp[i] = array[i];
        }

        array = temp;
    }

    private void reduceArray(int qtyOfReducing) throws IOException {

        String log = new Date() + "\tmethod -> reduceArray" + "\tparams -> " + qtyOfReducing + "\n";

        writeLogToFile(log);

        int[] temp = new int[array.length - qtyOfReducing];

        for (int i = 0; i < temp.length; i++) {

            temp[i] = array[i];
        }
        array = temp;
    }

    public void showElementsInRightOrder() throws IOException {

        String log = new Date() + "\tmethod ->  showElementsInRightOrder" +  "\n";

        writeLogToFile(log);

        for (int i = 0; i < array.length; i++) {

            System.out.print(array[i] + " ");
        }
    }

    public void showElementsReverse() throws IOException {
        String log = new Date() + "\tmethod ->  showElementsReverse" +  "\n";

        writeLogToFile(log);

        for (int i = array.length - 1; i >= 0; i--) {

            System.out.print(array[i] + " ");
        }
    }

    //bubble sort
    public void sort() throws IOException {
        String log = new Date() + "\tmethod ->  sort" +  "\n";

        writeLogToFile(log);

        for (int i = array.length - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j] > array[j + 1]) {

                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
            System.out.println(array[i]);
        }

    }

    /*

    4 1 5 0 3 6

    * i = 5; :
    *
    * j = 0; array[0] = 4 > array[0 + 1] = 1
    * j = 1; array[1] = 1 > array[1 + 1] = 5
    * j = 2; array[2] = 5 > array[2 + 1] = 0 ->
    *
    * tmp = 5;
    *
    * array[j] = 0
    * array[j+1] = 5;
    *
    *  4 1 5 0 3 6 -> 4 1 0 5 3 6
    * */
    private void deleteDuplicate() throws IOException {

        String log = new Date() + "\tmethod ->  deleteDuplicate" +  "\n";

        writeLogToFile(log);

        for (int i = 0; i < array.length; i++) {

            for (int j = i + 1; j < array.length; j++) {

                if (array[i] == array[j] && array[i] != 0) {

                    deleteElementByIndex(j);
                }

            }

        }

    }

    public void addArray(int[] newArray) throws IOException {

        String log = new Date() + "\tmethod ->  addArray" + "\tparams-> " + Arrays.toString(newArray)+  "\n";

        writeLogToFile(log);

        int countOfZeros = 0;

        for (int elem : array) {
            if (elem == 0) {
                countOfZeros++;
            }
        }

        int positionZero = array.length - countOfZeros;//1 5 7 0 -> 4 - 1 = 3


        int diff = countOfZeros - newArray.length;

        if (diff < 0) {
            increaseArray(Math.abs(diff));
        }

        // 1 5 7 0 0 0 -> 6 - 1 = 5
        for (int i = positionZero, j = 0; j < newArray.length; i++, ++j) { //10, 4
            array[i] = newArray[j];
        }

    }


    public void writeLogToFile(String log) throws IOException {

        Files.write(path, log.getBytes(), StandardOpenOption.WRITE, StandardOpenOption.APPEND, StandardOpenOption.CREATE);

    }



}


