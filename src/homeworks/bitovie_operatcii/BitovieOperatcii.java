package homeworks.bitovie_operatcii;

public class BitovieOperatcii {

    public static void main(String[] args) {
        // для чисел 41 и 23 выполнить побитовые операции &, |
        System.out.println(Integer.toBinaryString(41)); //00101001
        System.out.println(Integer.toBinaryString(23)); //00010111
        System.out.println("41 & 23 = " + (41 & 23));      //00000001
       // -------------------------------------------------------------
        System.out.println(Integer.toBinaryString(41)); //00101001
        System.out.println(Integer.toBinaryString(23)); //00010111
        System.out.println("41 | 23 = " + (41 | 23));      //00111111


        // для чисел 41 и 23 выполнить побитовые сдвиги
        System.out.println("41 << 2 = " + (41 << 2)); //00101001
        System.out.println("41 >> 2 = " + (41 >> 2));
        //00101001 << 2 -> 10100100; 41*2(2)= 164
        //00101001 >> 2 -> 00001010 ; 41/2(2)= 10;
        System.out.println("23 << 2 = " +  (23 << 2)); //00010111
        System.out.println("23 >> 2 = " + (23 >> 2));
        //00010111 << 2 -> 01011100; 23*2(2)= 92;
        //00010111 >> 2 -> 00000101; 23/2(2) = 5;

    }

}
