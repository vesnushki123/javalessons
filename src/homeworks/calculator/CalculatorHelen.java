package homeworks.calculator;

import java.util.Random;

public class CalculatorHelen {

    public void print() {
        System.out.println("Hello world");
    }

    public int add() {

        return getFirstParam() + getSecondParam();

    }

    public int callPrivate() {
        return privateMethod();
    }

    private int privateMethod() {
        return new Random().nextInt();
    }

    public final int finalMethod() {
        return new Random().nextInt();
    }

    public static int staticMethod() {
        return new Random().nextInt();
    }

    public final int subtraction() {

        return getFirstParam() - getSecondParam();

    }

    public int multiplication() {

        return getFirstParam() * getSecondParam();

    }

    public int division() {

        if (getSecondParam() == 0) {

            System.out.println("Shouldn't divide by zero!!");
        }

        return getFirstParam() / getSecondParam();

    }

    public int calculationOfpercent() { // считает процент от числа

        return getFirstParam() * getSecondParam() / 100;

    }

    public int getFirstParam() {
        return new Random().nextInt(20);
    }

    public int getSecondParam() {
        return new Random().nextInt(20);
    }

    public static void main(String[] args) {
        CalculatorHelen calc = new CalculatorHelen();
//        calc.calculationOfpercent();
    }


}
