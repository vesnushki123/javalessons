package homeworks.calculator;

import java.util.Random;

public class Calculator {

    public int add() {

        return getFirstParam() + getSecondParam();

    }

    public int subtraction() {

        return getFirstParam() - getSecondParam();
    }

    public int multiplication() {

        return getFirstParam() * getSecondParam();

    }

    public int division() {

        if (getSecondParam() == 0) {
            System.out.println("Shouldn't divide by zero!!");
        }

        return getFirstParam() / getSecondParam();

    }

    public int calculationOfpercent() { // считает процент от числа

        return (getFirstParam() * getSecondParam()) / 100;


    }

    public int getFirstParam() {
        return new Random().nextInt(20);
    }

    public int getSecondParam() {
        return new Random().nextInt(20);
    }

    public static void main(String[] args) {

        Calculator cacl = new Calculator();
        cacl.add();

    }


}
