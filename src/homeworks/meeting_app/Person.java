package homeworks.meeting_app;

/*
Создать приложение, позволяющие людям знакомиться.
Приложение должно позволять:
1) Регистрироваться человеку старше 18 лет.+
2) После регистрации выводить список подходящих мужчин/ женщин для этого человека по возрасту.+/-
3) Просматривать зарегистрировавшихся людей. Для мужчин выводить только женщин и наоборот.
4) Просматривать анкету отдельного человека(поиск по имени и фамилии)+
5) Организовать "умный поиск". Пользователь вводит требования(город, пол, возраст, количество детей) и  выводить людей,
которые соответствуют требованиям.+/-
 */
//Domain
public class Person {
    private int age;
    private City city;
    private Gender gender;
    private String firstName;
    private String lastName;
    private int countOfChildren;

    public Person(int age, Gender gender, City city, String firstName, String lastName, int countOfChildren) {
        this.age = age;
        this.gender = gender;
        this.city = city;
        this.firstName = firstName;
        this.lastName = lastName;
        this.countOfChildren = countOfChildren;

    }

    public int getAge() {
        return age;
    }

    public Gender getGender() {
        return gender;
    }

    public City getCity() {
        return city;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getCountOfChildren() {
        return countOfChildren;
    }

    public void printFields() {
        System.out.println("Age = " + age + " First name = " + firstName + "Last name = " + lastName);
    }
}
