package homeworks.meeting_app;

public class PersonService {
    private Person[] people;

    public PersonService() {
        people = new Person[10];//nulls
        people[0] = new Person(19, Gender.FEMALE, City.LOS_ANGELES,
                "Karl", "Smith", 5);
    }

    private void resize() {
        boolean shouldResize = false;

        for (Person element : people) {
            if (element == null) {
                shouldResize = true;
                break;
            }
        }

        if (!shouldResize) {
            Person[] temp = new Person[people.length * 2];//0

            for (int i = 0; i < people.length; i++) {
                temp[i] = people[i];
            }

            people = temp;
        }
    }

    public void registrationNewUser(Person person) {

        if (person.getAge() < 18) {
            System.out.println("You are younger than available age!");
            return;
        }

        resize();

        for (int i = 0; i < people.length; i++) {
            if (people[i] == null) {
                people[i] = person;
                break;
            }
        }

        showSuitablePersons(person.getAge(), person.getGender());//add gender
    }

    private void showSuitablePersons(int age, Enum gender) {
        for (Person p : people) {
            if (p.getAge() == age && p.getGender() != gender) {
                p.printFields();
            }
        }
    }

    public void showAllPersons(Gender gender) {
        for (Person p : people) {
            if (p.getGender() != gender) {
                p.printFields();
            }
        }

    }

    public void showPersonByFirstNameLastName(String firstName, String lastName) {
        for (Person p : people) {
            if (p.getFirstName().equals(firstName) && p.getLastName().equals(lastName)) {
                p.printFields();
            }
        }
    }

    public void smartSearch(City city, Gender gender, int age, int countOfChildren) {
        for (Person p : people) {

            if (p.getGender() == gender &&
                    p.getAge() == age &&
                    p.getCity().equals(city) &&
                    p.getCountOfChildren() == countOfChildren) {

                p.printFields();
            }
        }
    }
}
