package homeworks.meeting_app;

public class TestMeeting {
    public static void main(String[] args) {
        PersonService service = new PersonService();

        Person person = new Person(19, Gender.FEMALE, City.LOS_ANGELES,
                                    "Karl", "Smith", 5);

        service.registrationNewUser(person);
    }
}
