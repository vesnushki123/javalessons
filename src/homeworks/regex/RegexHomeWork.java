package homeworks.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexHomeWork {
    public static void main(String[] args) {
       /* 1) Любое количество букв, а потом две цифры и наоборот.
        String regex = "[A-Za-z]*[0-9]{2}";
        String text = "hellosefgtsrgs45";
        System.out.println(Pattern.matches(regex,text));

        String regex1 = "[0-9]*[a-zA-Z]{2}";
        String text1 = "465665645kk";
        System.out.println(Pattern.matches(regex1,text1));*/

       /* 2) 2-4 цифры, а потом 2-4 буквы и наоборот.
        String regex = "[0-9]{2,4}[a-zA-Z]{2,4}";
        String text = "5588qwet";
        System.out.println(Pattern.matches(regex,text));

        String regex1 = "[a-zA-Z]{2,4}[0-9]{2,4}";
        String text1 = "qwet22";
        System.out.println(Pattern.matches(regex1,text1));*/

       /* 3) Пользователь вводит имя и фамилию. Буквы могут быть в разных регистрах.
        Проверить, чтобы первые буквы были в верхнем регистре, а остальные буквы - в нижнем
        String regex1 = "[A-Z][a-z]*\\s[A-Z][a-z]*";
        String text1 = "Elena Belaya";
        System.out.println(Pattern.matches(regex1,text1));*/

       /* 4) В строке содержутся слова и числа. Необходмо выделить числа и посчитать их сумму.*/


        String regex = "[.*\\d.*]";

        Pattern pattern = Pattern.compile(regex);

        String text = "Hello55 world55";

        Matcher matcher = pattern.matcher(text);

        int count = 0;

        while (matcher.find()) {

            count += Integer.parseInt(matcher.group());

        }
        System.out.println(count);

      /*  5) Пользователь вводит телефон в формате (ххх)ххх-хх-хх.
        (ххх) может быть таким: (095), (097), (073), (067), (099), (063).
                Проверить, чтобы 7 - ый номер был в формате "(ххх)ххх-хх-хх".
                Если пользователь вводит номер начинающийся на(095) или (099) ххх-хх-хх, выводить в консоль "Пользователь имеет МТС номер",
                на(097) или (067) ххх-хх-хх, выводить в консоль "Пользователь имеет Киевстар номер",
                на(073) или (063) ххх-хх-хх, выводить в консоль "Пользователь имеет Лайф номер".*/

    }
}
