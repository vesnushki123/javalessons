package homeworks.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/*  5) Пользователь вводит телефон в формате (ххх)ххх-хх-хх.
        (ххх) может быть таким: (095), (097), (073), (067), (099), (063).
                Проверить, чтобы 7 - ый номер был в формате "(ххх)ххх-хх-хх".
                Если пользователь вводит номер начинающийся на(095) или (099) ххх-хх-хх, выводить в консоль "Пользователь имеет МТС номер",
                на(097) или (067) ххх-хх-хх, выводить в консоль "Пользователь имеет Киевстар номер",
                на(073) или (063) ххх-хх-хх, выводить в консоль "Пользователь имеет Лайф номер".*/
public class RegexPhone {

    public static void main(String[] args) {
        String regex = "(\\(067\\)|\\(095\\)|\\(097\\)|\\(073\\)|\\(099\\)|\\(063\\))\\d{3}-\\d{2}-\\d{2}";

        String text = "(067)387-67-81";

        String regexCode = "(\\(\\d{3}\\))";
//        System.out.println(Pattern.matches(regex, text));

        Pattern mobileOperator = Pattern.compile(regexCode);

        Matcher matcher = mobileOperator.matcher(text);

      //  Matcher matcher1 = mobileOperator.matcher(text + "Hello");


        String code = "another";

        if (matcher.find()) {
            code = matcher.group();
        }

        switch (code) {
            case"(067)":
                System.out.println("User have Kievstar mobile operator");


        }
    }

}
