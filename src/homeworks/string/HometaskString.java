package homeworks.string;

public class HometaskString {

    StringBuilder current_word;


    public void searchLongestWord(String sentence) {

        String[] s = sentence.split(" ");

        String maxWord = s[0];

        for (int i = 0; i < s.length; i++) {

            if (s[i].length() > maxWord.length()) {

                maxWord = s[i];
            }
        }

        System.out.println(maxWord);
    }

    public void searchShortestWord(String sentence) {

        String[] s = sentence.split(" ");

        String minWord = s[0];

        for (int i = 0; i < s.length; i++) {

            if (s[i].length() < minWord.length()) {

                minWord = s[i];
            }
        }
        System.out.println(minWord);
    }

    public void reverseSentence(String sentence) {

        /*char[] tempChar = sentence.toCharArray();

        for (int i = tempChar.length - 1; i >= 0; i--) {

            System.out.print(tempChar[i]);

        }*/

        char[] str = "Hello my friend".toCharArray();

        for (int i = 0; i < str.length / 2; i++) {
            char temp = str[i];

            str[i] = str[str.length - 1 - i];

            str[str.length - 1 - i] = temp;
        }

    }


    public void countQtyOfRepeatWordsWithoutRepeating(String sentence) {

        String[] s = sentence.split(" ");

        for (int i = 0; i < s.length; i++) {

            String word = s[i];

            sentence.contains(s[i]);

            int count = 0;

            for (int j = 0; j < s.length; j++) {

                if (word.equals(s[j])) {
                    count++;
                }
            }

            System.out.println("Word " + word + " = " + count);

        }

    }

    public void countQtyOfRepeatWordsWithRepeating(String sentence) {

        String[] s = sentence.split(" ");

        for (int i = 0; i < s.length; i++) {

            String word = s[i];

            int count = 1;//hello my friend hello

            int position = sentence.indexOf(word);

            int wordLength = word.length(); //error were here, delete + 1

            String temp = sentence.substring(position + wordLength);

            while (temp.contains(word)) {

                int tempPosition = temp.indexOf(word);

                temp = temp.substring(tempPosition + wordLength);

                count++;
            }

            for (int j = 0; j < s.length; j++) {
                if (word.equals(s[j])) {//error were here
                    s[j] = " ";
                }
            }

            if (!word.equals(" ")) {
                System.out.println("Word " + word + " = " + count);
            }
        }

    }

    public static void main(String[] args) {

        HometaskString ht = new HometaskString();

        ht.countQtyOfRepeatWordsWithRepeating("hello test test world hello world hello");
    }

}


