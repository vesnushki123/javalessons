package homeworks.data;

public class LocalDateTimeMethods {

//    atZone
//    public ZonedDateTime atZone(ZoneId zone)
//    Combines this date-time with a time-zone to create a ZonedDateTime.
//    This returns a ZonedDateTime formed from this date-time at the specified time-zone.
//    The result will match this date-time as closely as possible.
//    Time-zone rules, such as daylight savings, mean that not every local date-time is valid for the specified zone,
//    thus the local date-time may be adjusted.

//    atOffset
//    public OffsetDateTime atOffset(ZoneOffset offset)
//    Combines this date-time with an offset to create an OffsetDateTime.
//    This returns an OffsetDateTime formed from this date-time at the specified offset.
//    All possible combinations of date-time and offset are valid.
//
//    from
//    public static LocalDateTime from(TemporalAccessor temporal)
//    Obtains an instance of LocalDateTime from a temporal object.
//    This obtains a local date-time based on the specified temporal.
//    A TemporalAccessor represents an arbitrary set of date and time information, which this factory converts to an instance of LocalDateTime.
//    The conversion extracts and combines the LocalDate and the LocalTime from the temporal object.
//    Implementations are permitted to perform optimizations such as accessing those fields that are equivalent to the relevant objects.

//    truncatedTo
//    public LocalDateTime truncatedTo(TemporalUnit unit)
//    Returns a copy of this LocalDateTime with the time truncated.
//    Truncation returns a copy of the original date-time with fields smaller than the specified unit set to zero.
//    For example, truncating with the minutes unit will set the second-of-minute and nano-of-second field to zero

//    range
//    public ValueRange range(TemporalField field)
//    Gets the range of valid values for the specified field.
//    The range object expresses the minimum and maximum valid values for a field.
//    This date-time is used to enhance the accuracy of the returned range.
//    If it is not possible to return the range, because the field is not supported or for some other reason, an exception is thrown.
}

