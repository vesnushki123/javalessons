package homeworks.data;

import org.mockito.cglib.core.Local;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

public class CalendarHW {

    private Event[] events;

    public CalendarHW() {

        events = new Event[15];
    }

    public Event[] getEvents() {

        return events;
    }

    public void showCurrentDateInTimeZone() {

        LocalDateTime dateTime = LocalDateTime.now();//mocking -> 13.03.18 21.00

        ZoneId kievZone = ZoneId.of("Europe/Kiev");

        ZoneId magadanZone = ZoneId.of("Asia/Magadan");

        ZoneId denverZone = ZoneId.of("America/Denver");

        ZonedDateTime kiev = ZonedDateTime.of(dateTime, kievZone);

        ZonedDateTime magadan = kiev.withZoneSameInstant(magadanZone);//

        ZonedDateTime denver = kiev.withZoneSameInstant(denverZone);//

        System.out.println(kiev + "\n" + denver + "\n" + magadan);


    }

    public void createEvent(LocalDate date, String nameEvent) {

        Event event = new Event(date, nameEvent);

        for (int i = 0; i < events.length; i++) {

            if (Objects.isNull(events[i])) {

                events[i] = event;

                break;
            }

        }
    }

    public void showEvents() {

        for (Event tempEvent : events) {

            if (!Objects.isNull(tempEvent)) {

                System.out.println(tempEvent);
            }
        }

    }


    public void showEventsByDay(LocalDate date) {

        for (Event event : events) {

            if (event.getDate().equals(date)) {

                System.out.println(event);
            }

        }
    }

    public void deleteEvent(LocalDate date) {

        for (int i = 0; i < events.length; i++) {

            if (date.equals(events[i].getDate())) {

                events[i] = null;

                break;
            }

        }

    }

    public void showTimeZoneAndDate(String timezone) {

        LocalDateTime date = LocalDateTime.now(ZoneId.of(timezone));

        LocalTime localTime = date.toLocalTime();

        DayOfWeek dayOfWeek = date.getDayOfWeek();

        System.out.println(localTime + " " + dayOfWeek);

    }

    public void showFutureDate(String data) {

        LocalDate date = LocalDate.now();

        LocalDate tempDate = null;

        switch (data) {
            case "week":

                tempDate = date.plusWeeks(1);

                break;

            case "month":
                LocalDate monthDate = LocalDate.now();

                LocalDate newMonthDate = monthDate.plusMonths(1);

                System.out.println(newMonthDate);

                break;

            case "year":
                LocalDate yearDate = LocalDate.now();

                LocalDate newYearDate = yearDate.plusYears(1);

                System.out.println(newYearDate);

                break;

        }

        System.out.println(tempDate);
    }

    public void showDateOrTime(String value) {

        LocalDateTime date = LocalDateTime.now();

        switch (value) {

            case "time":

                LocalTime localTime = date.toLocalTime();

                System.out.println(localTime);

                break;

            case "date":
                LocalDate data = LocalDate.now();

                System.out.println(data);

                break;

            case "day of week":

                System.out.println(date.getDayOfWeek());

                break;


            case "day of the year":

                System.out.println(date.getDayOfYear());

                break;


            case "days to new year":

                LocalDate today = LocalDate.now();

                LocalDate newYearDay = LocalDate.of(2019, Month.JANUARY, 1);

                long p = ChronoUnit.DAYS.between(today, newYearDay);

                System.out.println(p);

                break;


        }
    }

    public void showDateInFormat(String format) {

        LocalDateTime date = LocalDateTime.now();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);

        String formattedDateTime = date.format(formatter);

        System.out.println(formattedDateTime);

    }


    public static void main(String[] args) {

        CalendarHW hw = new CalendarHW();


        //hw.showDateInFormat("yyyy-MM-dd HH:mm:ss.n");

        hw.showTimeZoneAndDate("America/Los_Angeles");


    }
}
