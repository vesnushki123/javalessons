package homeworks.data;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class LocalDateMethods {


//
//    format
//    public String format(DateTimeFormatter formatter)
//    Formats this date using the specified formatter.
//    This date will be passed to the formatter to produce a string.

//    minus
//    public LocalDate minus(long amountToSubtract,
//                           TemporalUnit unit)
//    Returns a copy of this date with the specified amount subtracted.
//    This returns a LocalDate, based on this one, with the amount in terms of the unit subtracted.
//    If it is not possible to subtract the amount, because the unit is not supported or for some other reason, an exception is thrown.
//    This method is equivalent to plus(long, TemporalUnit) with the amount negated.
//    See that method for a full description of how addition, and thus subtraction, works.
//    This instance is immutable and unaffected by this method call.
//
//    now1
//    public static LocalDate now()
//    Obtains the current date from the system clock in the default time-zone.
//    This will query the system clock in the default time-zone to obtain the current date.
//
//    now2
//    public static LocalDate now(ZoneId zone)
//    Obtains the current date from the system clock in the specified time-zone.
//    This will query the system clock to obtain the current date. Specifying the time-zone avoids dependence on the default time-zone.
//
//    now3
//    public static LocalDate now(Clock clock)
//    Obtains the current date from the specified clock.
//    This will query the specified clock to obtain the current date - today.
//    Using this method allows the use of an alternate clock for testing.
//    The alternate clock may be introduced using dependency injection.

//    parse
//    public static LocalDate parse(CharSequence text)
//    Obtains an instance of LocalDate from a text string such as 2007-12-03.
//    The string must represent a valid date and is parsed using DateTimeFormatter.ISO_LOCAL_DATE.
//
//    public static LocalDate parse(CharSequence text,
//                                  DateTimeFormatter formatter)
//    Obtains an instance of LocalDate from a text string using a specific formatter.
//    The text is parsed using the formatter, returning a date.

//    plus
//    public LocalDate plus(TemporalAmount amountToAdd)
//    Returns a copy of this date with the specified amount added.
//    This returns a LocalDate, based on this one, with the specified amount added.
//    The amount is typically Period but may be any other type implementing the TemporalAmount interface.
//    The calculation is delegated to the amount object by calling TemporalAmount.addTo(Temporal).
//    The amount implementation is free to implement the addition in any way it wishes,
//    however it typically calls back to plus(long, TemporalUnit).
//    Consult the documentation of the amount implementation to determine if it can be successfully added.

//    of
//    public static LocalDate of(int year,
//                           Month month,
//                           int dayOfMonth)
//    Obtains an instance of LocalDate from a year, month and day.
//    This returns a LocalDate with the specified year, month and day-of-month.
//    The day must be valid for the year and month, otherwise an exception will be thrown.

      //until
//    public Period until(ChronoLocalDate endDateExclusive)
//    Calculates the period between this date and another date as a Period.
//    This calculates the period between two dates in terms of years, months and days.
//    The start and end points are this and the specified date. The result will be negative if the end is before the start.
//    The negative sign will be the same in each of year, month and day.

//    isAfter
//    public boolean isAfter(ChronoLocalDate other)
//    Checks if this date is after the specified date.
//    This checks to see if this date represents a point on the local time-line after the other date.

//    isBefore
//    public boolean isBefore(ChronoLocalDate other)
//    Checks if this date is before the specified date.
//    This checks to see if this date represents a point on the local time-line before the other date.


}
