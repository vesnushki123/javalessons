package homeworks.data;

import java.time.LocalDate;
import java.util.Objects;

public class Event {
    private LocalDate date;
    private String name;

    public Event(LocalDate date, String name) {
        this.date = date;
        this.name = name;
    }

    public LocalDate getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Event{" +
                "date=" + date +
                ", name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        return Objects.equals(date, event.date) &&
                Objects.equals(name, event.name);
    }

}
