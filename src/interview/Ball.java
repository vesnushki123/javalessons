package interview;

import java.util.ArrayList;
import java.util.List;

public class Ball {

    private String name;//has-a

    private int age;

    private String address;

    public final int COUNT;


    {

        System.out.println("Non-static block initialization");
    }

    static {
        System.out.println("Static block initialization");
    }

    public Ball(String name, int age) {
        COUNT = age;
        this.name = name;
        this.age = age;
    }

    public Ball(String name, int age, String address) {
        this(name, age);
        this.address = address;
    }

    public String getName() {
//        COUNT = 5;
        return name;
    }

    public static void foo(String name, int age) {

    }

    public static void foo(String name) {

    }

    public void setName(String name) {//"Hello", "Hello5"
//        foo();
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}

class TestBall {
    public static void main(String[] args) {
        Ball ball1 = new Ball("Name1", 45);//(this, "", 45)

        String name2 = "Name2";

        Ball ball2 = new Ball(name2, 45);

        ball2 = null;

        char c = 'D';

        c = '\u4589';

        c = 7894;

        List<Integer> integers = new ArrayList<>();

        integers.add(5);//new Integer(5)

        int a = new Integer(10);
    }
}
