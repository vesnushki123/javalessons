package lessons.collection;

import java.util.*;

public class CommonCollection {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();//LinkedList

        list.add(2);
        list.add(1);
        list.add(8);
        list.add(1);
        list.add(6);


//        list.trimToSize();

        new TestList(new ArrayList<>());
        new TestList(new LinkedList<>());

        for (int element : list) {
//            System.out.println(element);
        }

        for (int i = 0; i < list.size(); i++) {
//            System.out.println(list.get(i));
        }

        Iterator<Integer> iterator = list.iterator();

        /*while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }*/

        Set<Integer> set = new HashSet<>();

//        set = new LinkedHashSet<>();

        set.add(5);
        set.add(1);
        set.add(6);

//        System.out.println(set);

//        list = new LinkedList<>(set);

//        System.out.println(list);

        set = new TreeSet<>();


        Queue<Integer> queue = new LinkedList<>();

        queue.add(5);
        queue.add(null);

    }
}

class TestList {
    private List<Integer> integers;

    public TestList(List<Integer> integers) {
        this.integers = integers;
    }
}
