package lessons.overloading_overriding;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Path;
import java.sql.SQLDataException;

public class Furniture {
    protected void print() {
        System.out.println("Furniture");
    }

    public void getNumber(String s) {

    }

    public Object getFileName(Path path) throws IOException {
        File file = new File("./Text.txt");
        return file;
    }

    public static void changeInt(int a) {
        System.out.println("int Furniture");
    }

    public static void changeInt(Integer a) {
        System.out.println("Integer");
    }

    public static void main(String[] args) {
        changeInt(new Integer(5));
    }

}

class Sofa extends Furniture {

    @Override
    public void getNumber(String v) {

    }

    public File getFileName(Path path) throws FileNotFoundException, FileAlreadyExistsException {
        return new File("./MyFile.txt");
    }

    /*public static void changeInt(int a) {
        System.out.println("int Sofa");
    }*/

    @Override
    public void print() {
        System.out.println("Sofa");
    }

    public static void main(String[] args) {
       Sofa.changeInt(5);
    }
}


/*
* private - visibility only inside class
* package-private(default) - visibility only inside package
* protected - visibility inside package + childs
* public - visibility anywhere
* */
