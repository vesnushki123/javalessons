package lessons.inheritance.access_modificators;

import lessons.inheritance.access_modificators.test.Animal;

public class Mouse extends Animal {
    public void printMouse() {
        super.print();
    }

    @Override
    public void say() {
        System.out.println("Pi-pi");
    }

    @Override
    public void print() {
        super.print();
    }
}
