package lessons.inheritance.access_modificators.test;

public class Animal {
    public void print() {

    }

    public static void staticMethod() {

    }

    public void say() {
        System.out.println("Say something");
    }
}


final class UnMutableClass {
    private int age;

    public UnMutableClass(int age) {
        this.age = age;
    }
}