package lessons.inheritance.access_modificators;

import lessons.inheritance.access_modificators.test.Animal;

public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal();

        Animal mouse = new Mouse();//new Animal

        Animal lion = new Lion();//new Animal

        mouse.say();

        lion.say();

        Lion.staticMethod();

//        int a = "";

//        mouse.print();
    }
}

/*
* private - visibility only inside class
* package-private(default) - visibility only inside package
* protected - visibility inside package + childs
* public - visibility anywhere*
* */
