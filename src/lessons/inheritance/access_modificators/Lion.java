package lessons.inheritance.access_modificators;

import lessons.inheritance.access_modificators.test.Animal;

public class Lion extends Animal {

    @Override
    public void say() {
        System.out.println("R-r");
    }
}
