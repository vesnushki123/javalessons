package lessons.inheritance;

import java.io.Serializable;

public class Mercedes extends Car {//is - a

    private String engineMercedes;//has - a

    public Mercedes(String name, String model, String engineMercedes) {
        super(name, model);
        this.engineMercedes = engineMercedes;
    }

    public String getEngineMercedes() {
        return engineMercedes;
    }
}

class Bmw extends Car {

    private String engineBmw;

    public Bmw(String name, String model, String engineBmw) {
        super(name, model);
        this.engineBmw = engineBmw;
    }

    /*@Override
    public void drive() {

    }

    @Override
    public void run() {

    }

    @Override
    public void abstractMethod() {

    }*/

    public String getEngineBmw() {
        return engineBmw;
    }

    @Override
    public void print() {
        System.out.println("Mercedes");
    }
}

interface Drivable extends Runnable {

    int AGE = 5;

    void drive();

    public static void runSome() {
    }

    default void printInfo() {
        System.out.println();
    }
}

 class Car implements Drivable {//parent or super class for Mercedes and Bmw
    private String name;

    private String model;

    public Car(String name, String model) {
        this.name = name;
        this.model = model;
    }

//    abstract void abstractMethod();

    @Override
    public void drive() {
        System.out.println("Drive Car");
    }

    @Override
    public void run() {
        System.out.println();
    }

    public String getName() {
        return name;
    }

    public String getModel() {
        return model;
    }

    public void print() {
        System.out.println("Car");
    }
}

class TestInh {
    public static void main(String[] args) {
//        Runnable mercedes = new Mercedes("Mercedes", "X5", "EngineMercedes");

        String text1 = "test";
        String text2 = "test";

        System.out.println(text1.equals(text2));
    }
}


