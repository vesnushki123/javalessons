package lessons.date;

import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class Dates {
    public static void main(String[] args) {

        long ms = System.currentTimeMillis();

        Date date = new Date(1172304813000L);

//        System.out.println(date);

        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Pacific/Honolulu"));

//        Date time = calendar.getTime();

//        calendar.add(Calendar.MONTH, 2);
//        calendar.setTimeZone(TimeZone.getTimeZone("Pacific/Honolulu"));

//        System.out.println(calendar.getTime());

        LocalTime time = LocalTime.now();

        System.out.println(time);
    }
}
