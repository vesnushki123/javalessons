package lessons.increment_decrement;

public class Counter {
    public static void main(String[] args) {
        int count = 2;

//        int result = ++count + ++count;//3 + 4
        int result = count++ + ++count;//2 + 4

//        System.out.println(result);
    }
}
