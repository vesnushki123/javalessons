package lessons.learn_exception;

import org.junit.contrib.java.lang.system.SystemOutRule;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLDataException;

//import static homeworks.file_manager.File_manager.path;
import static java.lang.Float.NaN;

public class LearnException {

    public static void throwException() {
        try {
            int e = 10;

            throw new IOException("I'm IOE");
        } catch (IOException e) {
//            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public static void tryCatchFinally() throws IOException {
        try {
//            call to database
//            throw new IOException("I'm IOE");

            Files.createFile(Paths.get("./Run.txt"));

            System.out.println("Hello world");

//            System.exit(5);

//            Runtime.getRuntime().exit(5);

//            close connection to database
        } catch (IOException e) {
            System.out.println("Catch");
//            e.printStackTrace();

            Files.createFile(Paths.get("./Run.txt"));
        } finally {

            System.out.println("Finally");
//            close connection
        }
    }

    public static void throwException1() throws IOException {
        int e = 10;

        throw new IOException("I'm IOE");
    }

    public static void throwMultiple() throws FileAlreadyExistsException {
        try {
            if (1 == 1) {
                throw new IOException();
            } else {
                throw new FileNotFoundException();
            }
        } catch (FileNotFoundException e) {
            throw new FileAlreadyExistsException("");
        } catch (IOException e) {
           /* e = new FileNotFoundException();
            throw e;*/
        } catch (Exception e) {

        }
    }

    public static void useMultiCatch() {

       /* try () {

        } catch () {
        } finally {

        }*/

      /*  try (BufferedWriter writer = Files.newBufferedWriter(path)) {

            writer.write(s, 0, s.length());

        } catch () {
        }finally {

        }*/

        try {
            if (1 == 1) {
                throw new LenaException();
            } else if (2 != 2) {
                throw new SQLDataException();
            } else {
                throw new NullPointerException();
            }

        } catch (LenaException | SQLDataException | NullPointerException e) {
//            e = new FileNotFoundException();
        }
    }


    public static void main(String[] args) throws IOException {

//        throwException1();

        tryCatchFinally();

//        float a = NaN;

//        throw new NullPointerException("I'm NPE");
//        throw new IOException("I'm IOE");

     /*   Object o = null;

        o.hashCode();*/

    }
}

class LenaException extends IOException {
    public LenaException() {
    }

    public void printMessage() {
        System.out.println("Lena");
    }
}
