package lessons.for_test;

public class Calculator {

    public int sum(int a, int b) throws InterruptedException {
        int result = forsum(a, b);//method with hard logic
        return result;
    }

    public int forsum(int a, int b) throws InterruptedException {

        for (int i = 0; i < 10000000; i++) {
            Thread.sleep(500);
        }

        return a + b;
    }


    public static void main(String[] args) throws InterruptedException {
//        forsum(5, 6);
    }
}
