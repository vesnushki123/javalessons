package lessons.regex;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonRegex {
    public static void main(String[] args) {
    /*    String regex = "http://";

        String text = "http://g";*/

        // Matching any character

        /*String regex = "H.llo";

        String text = "Hbllo";*/

        // Matching set of characters

//        String regex = "H[as%]llo";

//        String regex = "H[a-sA-S]llo";
//        String regex = "H[a-sA-S0-9]llo";
//        String regex = "H[a-sA-S0-9&&[^f5]]llo"; //exclude some symbol from set
//        String regex = "H\\dllo";// any digits
//        String regex = "H\\Dllo";// any character
//        String regex = "H\\wllo";// shortcut for [a-zA-Z_0-9]
//        String regex = "H\\Wllo";// shortcut for #, %, &
//        String regex = "H(run|gun)llo";// or condition

//        Quantifiers
//        String regex = "H*llo";//multiple times H (include 0)
//        String regex = "H+llo";//multiple times (should be at least 1 time)
//        String regex = "H{2}llo";//should appears  2 times
//        String regex = "H{2,4}llo";//should appears from 2 to 4 times
//        String regex = "H?llo";//should appears 0 or 1 times
//        String regex = "H\\d{2,4}\\D+llo*";
//        String regex = "H([a-zA-Z&&[^rt2]]){1,2}ll(.*(try|catch).*)o_{3,4}";
//        DELETE FROM person where.... -> DELETE
        String regex = "H([a-zA-Z&&[^rt2]]){1,2}ll(.*(try|catch).*)o_{3,4}";

        String text = "HaBllttcatchxxo___";

        System.out.println(Pattern.matches(regex, text));
    }
}


class UsePatternClass {
    public static void main(String[] args) {
//        String regex = "h([a-zA-Z&&[^rt2]]){1,2}ll(.*(try|catch).*)o_{3,4}";
        String regex = "sep";

        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);

//        String text = "HaBllttcatchxxo___";
        String text = "A sep Text sep With Many sep";

        String[] split = pattern.split(text);

        System.out.println(Arrays.toString(split));

//        Matcher matcher = pattern.matcher(text);

//        System.out.println(matcher.matches());


    }
}

class UseMatcherClass {
    public static void main(String[] args) {
        String regex = "Hello";

        Pattern pattern = Pattern.compile(regex);

        String text = "Hello world";

        Matcher matcher = pattern.matcher(text);

        System.out.println(matcher.lookingAt());



        int count = 0;

        while (matcher.find()) {
            count += Integer.parseInt(matcher.group());
//            System.out.println("Found: " + matcher.group() + " start -> " + matcher.start() + " end -> " + matcher.end());
        }

//        System.out.println(count);

    }
}