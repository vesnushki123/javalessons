package lessons.learn_switch;

public class Switcher {
    public static void main(String[] args) {
        String a = "15";

        switch (a) {
            case "10":
                System.out.println("a == 10");
                break;

            case "11":
                System.out.println("a == 11");
                break;

            case "12":
                System.out.println("a == 12");
                break;
            default:
                System.out.println("a == 44");

        }
    }
}
