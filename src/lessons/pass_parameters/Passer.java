package lessons.pass_parameters;

public class Passer {

    public static void changeInt(int a) {//copy
        a = 15;
    }

    public static String changeString(String str) {
        str = "World";
       return str;//new String("World")
    }

    public static void changeMan(Man man) {
        man.name = "Gun";
    }

    public static void main(String[] args) {
        int r = 10;

        String string = "Hello";
        String string1 = "World";

        string = string1;

        int a = 10;

        int b = 7;

        a = b;

        Man man = new Man();

        man.name = "Run";

        changeMan(man);

        System.out.println(man.name);

        /*final char[] chars = new char[10];

        chars = new char[1];*/

        changeInt(r);//int rCopy = 10;

        string = changeString(string);

        System.out.println(string);//pass copy of variable string

//        System.out.println(r);
    }
}
