package lessons.work_with_file;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.*;

public class FileWorker {
    public static void main(String[] args) throws IOException {

//        Path path = Paths.get("./Algorithm.txt");
        Path path = FileSystems.getDefault().getPath("./Algorithm.txt");

        if (Files.exists(path)) {

            BufferedReader bufferedReader = Files.newBufferedReader(path);

            String line = null;

            String nameOfAlg = null;

            while ((line = bufferedReader.readLine()) != null) {
                nameOfAlg = line;
                System.out.println(line);
            }

            if (nameOfAlg.equals("Moscow")) {
                System.out.println("There is Moscow line");
            }
        }
    }
}
