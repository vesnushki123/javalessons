package lessons.learn_class;


public class Man {
    private String name;
    private int age;

    public static final String LAST_NAME = "LastName";

    {//before constructor
        System.out.println("Non-static block initialization");
    }

    static {//while loading class
//        System.out.println("Static block initialization");
//        System.out.println("Hello world");
    }


    public Man(String name, int age) {
//        lastName = "45";
        System.out.println("Constructor Man");
        this.name = name;
        this.age = age;
    }

    public static int print(int a) {
        a = 20;
        return a;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {//(this, age)
        this.age = age;
    }
}


class TestMan {
    public static void main(String[] args) {

        int k = 15;


        Man.print(k);//int a1 = a;

        System.out.println(k);
//        Man man = new Man("Man", 12);
//        man = null;
//        Man man1 = new Man("Man1", 22);
//        man1 = man;
//        man.setAge(55);//(this, 55)

//        man1.setAge(55);//(this, 55)
 /*       man.age = 8955456;



        System.out.println(man.age);*/
    }
}
