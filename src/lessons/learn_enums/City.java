package lessons.learn_enums;

public enum City {
    DNEPR("Dnepr"), KIEV("Kiev");

    private String shortName;

    City(String shortName) {
        this.shortName = shortName;
    }

    public String getShortName() {
        return shortName;
    }
}

class TestEnum {

    private City city;

    public TestEnum(City city) {
        this.city = city;
    }

    public City getCity() {
        return city;
    }

    public void foo(String city) {//"DnePr", "Kiev"

    }

    public static void foo(City city) {

    }

    public static void main(String[] args) {
        City dnepr = City.DNEPR;
//        System.out.println(dnepr.getShortName());

        for (City city : City.values()) {
//            System.out.println(city);
        }

        String str = "Dnepr";

        City dn = City.valueOf(str.toUpperCase());

        TestEnum testEnum = new TestEnum(City.KIEV);

        if (dn == City.DNEPR) {

        }

    }
}
